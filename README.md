# Split wise Application

### Create users in system with number of users followed by user ids and email

```
4
1 a@udaan.com
2 b@udaan.com
3 c@udaan.com
4 d@udaan.com
``` 

### Find Balance for all users

```
BALANCE
```

### Find Balance for a user followed by user id

```
BALANCE 2
```

### Add Exact Expense followed by paid user id, amount, number of users, user ids (=number of users), shares (=number of users)

```
EXPENSE EXACT 1 100 4 1 2 3 4 10 20 30 40
```

### Add Percentage Expense followed by paid user id, amount, number of users, user ids (=number of users), shares (=number of users)

```
EXPENSE PERCENTAGE 1 100 4 1 2 3 4 10 20 30 40
```

### Add Direct payment made between 2 users, paid by user, paid to user and amount

```
PAYMENT 4 1 25
```

