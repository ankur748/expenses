package com.udaan.dao.users.impl;

import com.udaan.dao.users.UserDao;
import com.udaan.model.users.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDaoImpl implements UserDao {

    private static UserDao userDao;
    private Map<Long, User> userMap;

    private UserDaoImpl() {
        this.userMap = new HashMap<>();
    }

    public static UserDao getInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl();
        }
        return userDao;
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(this.userMap.values());
    }

    @Override
    public User getUserById(long userId) {
        return this.userMap.get(userId);
    }

    @Override
    public void addUser(long userId, String email) {
        this.userMap.put(userId, new User(userId, email));
    }
}
