package com.udaan.dao.users;

import com.udaan.model.users.User;

import java.util.List;

public interface UserDao {

    List<User> getAllUsers();
    User getUserById(long userId);
    void addUser(long userId, String email);

}
