package com.udaan.dao.balances;

import com.udaan.model.balances.UserBalances;
import com.udaan.model.balances.settlements.Settlement;

public interface BalanceDao {

    UserBalances getBalanceByUser(long userId);
    void addSettlement(Settlement settlement);

}
