package com.udaan.dao.balances.impl;

import com.udaan.dao.balances.BalanceDao;
import com.udaan.model.balances.UserBalances;
import com.udaan.model.balances.settlements.Settlement;

import java.util.HashMap;
import java.util.Map;

public class BalanceDaoImpl implements BalanceDao {

    private static BalanceDao balanceDao;
    private Map<Long, UserBalances> userBalancesMap;

    private BalanceDaoImpl() {
        this.userBalancesMap = new HashMap<>();
    }

    public static BalanceDao getInstance() {
        if (balanceDao == null) {
            balanceDao = new BalanceDaoImpl();
        }
        return balanceDao;
    }

    @Override
    public UserBalances getBalanceByUser(long userId) {
        return this.userBalancesMap.getOrDefault(userId, new UserBalances(userId));
    }

    @Override
    public void addSettlement(Settlement settlement) {

        long payerId = settlement.getPaidByUserId();
        long beneficiaryId = settlement.getPaidForUserId();

        if (!userBalancesMap.containsKey(payerId)) {
            userBalancesMap.put(payerId, new UserBalances(payerId));
        }
        userBalancesMap.get(payerId).addSettlement(settlement);

        //reverse settlement
        Settlement reverseSettlement = settlement.getReverseSettlement();

        if (!userBalancesMap.containsKey(beneficiaryId)) {
            userBalancesMap.put(beneficiaryId, new UserBalances(beneficiaryId));
        }
        userBalancesMap.get(beneficiaryId).addSettlement(reverseSettlement);

    }
}
