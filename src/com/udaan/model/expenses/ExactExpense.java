package com.udaan.model.expenses;

import com.udaan.model.balances.settlements.Settlement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExactExpense extends Expense {

    private final List<Long> userShares;

    public ExactExpense(Long paidByUser, List<Long> paidForUsers, long amount,
                        List<Long> userShares) {
        super(paidByUser, paidForUsers, amount);
        this.userShares = new ArrayList<>(userShares);
    }

    public List<Long> getUserShares() {
        return Collections.unmodifiableList(userShares);
    }

    @Override
    public List<Settlement> doSettlements() {

        List<Settlement> settlements = new ArrayList<>();

        for (int i=0; i<getPaidForUsers().size(); i++) {
            if (getPaidForUsers().get(i) == getPaidByUser()) {
                continue;
            }
            settlements.add(new Settlement(getPaidByUser(),
                    getPaidForUsers().get(i), getUserShares().get(i)));
        }

        return settlements;

    }
}
