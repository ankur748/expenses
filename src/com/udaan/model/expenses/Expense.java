package com.udaan.model.expenses;

import com.udaan.model.balances.settlements.Settlement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Expense {

    private Long paidByUser;
    private List<Long> paidForUsers;
    private long amount;

    public Expense(Long paidByUser, List<Long> paidForUsers, long amount) {
        this.paidByUser = paidByUser;
        this.paidForUsers = new ArrayList<>(paidForUsers);
        this.amount = amount;
    }

    public Long getPaidByUser() {
        return paidByUser;
    }

    public List<Long> getPaidForUsers() {
        return Collections.unmodifiableList(paidForUsers);
    }

    public long getAmount() {
        return amount;
    }

    public abstract List<Settlement> doSettlements();
}
