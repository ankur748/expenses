package com.udaan.model.expenses;

import com.udaan.model.balances.settlements.Settlement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PercentageExpense extends Expense {

    private final List<Long> userSharePercentages;

    public PercentageExpense(Long paidByUser, List<Long> paidForUsers, long amount,
                             List<Long> userSharePercentages) {
        super(paidByUser, paidForUsers, amount);
        this.userSharePercentages = new ArrayList<>(userSharePercentages);
    }

    public List<Long> getUserSharePercentages() {
        return Collections.unmodifiableList(userSharePercentages);
    }

    @Override
    public List<Settlement> doSettlements() {
        List<Settlement> settlements = new ArrayList<>();

        for (int i=0; i<getPaidForUsers().size(); i++) {
            if (getPaidForUsers().get(i) == getPaidByUser()) {
                continue;
            }
            settlements.add(new Settlement(getPaidByUser(), getPaidForUsers().get(i),
                    (double)getUserSharePercentages().get(i)*getAmount()/100));
        }

        return settlements;
    }
}
