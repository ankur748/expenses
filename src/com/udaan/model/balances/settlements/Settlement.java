package com.udaan.model.balances.settlements;

public class Settlement {

    private final long paidByUserId;
    private final long paidForUserId;
    private final double amount;

    public Settlement(long paidByUserId, long paidForUserId, double amount) {
        this.paidByUserId = paidByUserId;
        this.paidForUserId = paidForUserId;
        this.amount = amount;
    }

    public long getPaidByUserId() {
        return paidByUserId;
    }

    public long getPaidForUserId() {
        return paidForUserId;
    }

    public double getAmount() {
        return amount;
    }

    public Settlement getReverseSettlement() {
        return new Settlement(paidForUserId, paidByUserId, -amount);
    }

}
