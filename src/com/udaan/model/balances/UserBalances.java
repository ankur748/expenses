package com.udaan.model.balances;

import com.udaan.model.balances.settlements.Settlement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class UserBalances {

    private final long userId;
    private final Map<Long, Double> balances;

    public UserBalances(long userId) {
        this.userId = userId;
        balances = new HashMap<>();
    }

    public void addSettlement(Settlement settlement) {

        double newBalance = balances.getOrDefault(settlement.getPaidForUserId(), 0.00)
                + settlement.getAmount();

        if (newBalance == 0 && balances.containsKey(settlement.getPaidForUserId())) {
            balances.remove(settlement.getPaidForUserId());
        } else {
            balances.put(settlement.getPaidForUserId(), newBalance);
        }
    }

    public Map<Long, Double> getBalances() {
        return Collections.unmodifiableMap(balances);
    }
}
