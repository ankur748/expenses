package com.udaan.manager;

import com.udaan.constants.ExpenseType;
import com.udaan.dao.balances.impl.BalanceDaoImpl;
import com.udaan.dao.users.impl.UserDaoImpl;
import com.udaan.model.balances.UserBalances;
import com.udaan.model.balances.settlements.Settlement;
import com.udaan.model.expenses.ExactExpense;
import com.udaan.model.expenses.Expense;
import com.udaan.model.expenses.PercentageExpense;
import com.udaan.model.users.User;

import java.util.List;
import java.util.Map;

public class ExpenseManager {

    public void addUserToSystem(long userId, String email) {
        UserDaoImpl.getInstance().addUser(userId, email);
    }

    public void printBalancesForAllUsers() {
        List<User> users = UserDaoImpl.getInstance().getAllUsers();

        for (User user : users) {
            printBalancesForUser(user.getId());
        }
    }

    public void printBalancesForUser(long userId) {

        UserBalances userBalances = BalanceDaoImpl.getInstance()
                .getBalanceByUser(userId);

        String paidByUserEmail = UserDaoImpl.getInstance().getUserById(userId).getEmail();

        for (Map.Entry<Long, Double> entry : userBalances.getBalances().entrySet()) {

            String paidForUserEmail = UserDaoImpl.getInstance().getUserById(entry.getKey()).getEmail();
            Double paidAmount = entry.getValue();

            if (paidAmount > 0 ) {
                System.out.println(paidForUserEmail + " owes user " + paidByUserEmail
                        + " amount of " + paidAmount);
            } else {
                System.out.println(paidByUserEmail + " owes user " + paidForUserEmail
                        + " amount of " + Math.abs(paidAmount));
            }
        }

    }

    public void addExpense(ExpenseType expenseType, long paidByUserId, long paidAmount,
                           List<Long> paidForUsersList, List<Long> data) {
        Expense expense;
        switch (expenseType) {
            case EXACT:
                expense = new ExactExpense(paidByUserId, paidForUsersList, paidAmount,
                        data);
                break;
            case PERCENTAGE:
                expense = new PercentageExpense(paidByUserId, paidForUsersList, paidAmount,
                        data);
                break;
            default:
                throw new RuntimeException("expense type not supported");
        }

        List<Settlement> settlements = expense.doSettlements();

        for (Settlement settlement : settlements) {
            BalanceDaoImpl.getInstance().addSettlement(settlement);
        }

    }

    public void addDirectPayment(long paidByUserId, long paidToUserId, long paidAmount) {

        Settlement settlement = new Settlement(paidByUserId, paidToUserId, paidAmount);
        BalanceDaoImpl.getInstance().addSettlement(settlement);
    }

}
