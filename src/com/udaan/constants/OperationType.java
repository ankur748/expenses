package com.udaan.constants;

public enum OperationType {
    BALANCE("BALANCE"), EXPENSE("EXPENSE"), PAYMENT("PAYMENT");

    private String operationType;

    OperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationType() {
        return this.operationType;
    }
}
