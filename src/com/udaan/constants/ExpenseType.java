package com.udaan.constants;

public enum ExpenseType {

    EXACT("EXACT"), PERCENTAGE("PERCENTAGE");

    private String expenseType;

    ExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseType() {
        return expenseType;
    }
}
