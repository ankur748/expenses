package com.udaan;

import com.udaan.constants.ExpenseType;
import com.udaan.manager.ExpenseManager;
import com.udaan.constants.OperationType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        ExpenseManager expenseManager = new ExpenseManager();

        //Parse the user ids of the system
        int num_users = Integer.parseInt(in.readLine());
        for (int i=0; i<num_users; i++) {
            String[] strings = in.readLine().split(" ");
            expenseManager.addUserToSystem(Long.parseLong(strings[0]),
                    strings[1]);
        }

        //Parse commands and subsequent data
        while(true) {
            String[] arguments = in.readLine().split(" ");

            OperationType operationType =
                    OperationType.valueOf(arguments[0]);

            if (operationType == OperationType.BALANCE) {
                if (arguments.length == 1) {
                    expenseManager.printBalancesForAllUsers();
                } else {
                    expenseManager.printBalancesForUser(
                            Long.parseLong(arguments[1]));
                }
            } else if (operationType == OperationType.EXPENSE) {
                ExpenseType expenseType = ExpenseType.valueOf(arguments[1]);

                long paidByUserId = Long.parseLong(arguments[2]);
                long paidAmount = Long.parseLong(arguments[3]);

                int numUsersInvolved = Integer.parseInt(arguments[4]);
                List<Long> paidForUsersList = new ArrayList<>();
                for (int i = 5; i<= (4+numUsersInvolved); i++) {
                    paidForUsersList.add(Long.parseLong(arguments[i]));
                }

                List<Long> dataList = new ArrayList<>();
                for (int i=(5+numUsersInvolved); i<=(4+(2*numUsersInvolved)); i++) {
                    dataList.add(Long.parseLong(arguments[i]));
                }

                expenseManager.addExpense(expenseType, paidByUserId, paidAmount, paidForUsersList, dataList);
            } else if (operationType == OperationType.PAYMENT) {
                long paidByUserId = Long.parseLong(arguments[1]);
                long paidToUserId = Long.parseLong(arguments[2]);
                long paidAmount = Long.parseLong(arguments[3]);

                expenseManager.addDirectPayment(paidByUserId, paidToUserId, paidAmount);
            }
        }

    }
}
